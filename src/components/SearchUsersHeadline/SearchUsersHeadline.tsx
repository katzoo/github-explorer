import React from 'react';
import { Text } from '@chakra-ui/react';

interface Props {
  query: string;
}

export const SearchUsersHeadline: React.FC<Props> = ({ query }) => (
  <Text as="span">{`Showing users for "${query}"`}</Text>
);
