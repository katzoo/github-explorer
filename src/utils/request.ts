import fetch from 'cross-fetch';
import { z } from 'zod';

export class HttpError extends Error {}

export const makeCancellableFetch = <T>(url: string, opts?: RequestInit): Promise<T> => {
  const abortController = new AbortController();
  const { signal } = abortController;

  const promise = new Promise<T>(async (resolve, reject) => {
    try {
      const response = await fetch(url, {
        method: 'get',
        ...opts,
        signal,
      });

      if (response.ok) {
        const data = await response.json();
        resolve(data);
      } else {
        reject(new HttpError(`[HttpError] - ${response.status} ${response.statusText}`));
      }
    } catch (err) {
      if (import.meta.env.DEV) {
        console.log('[makeCancellableFetch error]', err);
      }
      // NOTE(m.kania): request wasn't cancelled, so reject
      if (err instanceof Error && err.name !== 'AbortError') {
        reject(err);
      }
    }
  });

  // @ts-ignore
  promise.cancel = () => abortController.abort();

  return promise;
};

export const makeValidatedCancellableFetch =
  <ValidatorType>(validator: z.ZodType<ValidatorType>, opts?: RequestInit) =>
  (...params: Parameters<typeof makeCancellableFetch>) => {
    const [url, paramsOpts] = params;
    const response = makeCancellableFetch(url, { ...opts, ...paramsOpts });

    const promise = response.then((data) => validator.parse(data));

    // @ts-ignore
    promise.cancel = response.cancel;

    return promise;
  };
