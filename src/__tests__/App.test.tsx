import { describe, it, expect } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import { server, rest } from '~/test/test-server';

import App from '../App';
import { API_URLS } from '~/domain/users';

const setup = () => {
  const utils = render(<App />);
  const input = utils.getByLabelText('Username') as HTMLInputElement;
  const searchButton = utils.getByText('Search');

  return {
    input,
    searchButton,
    ...utils,
  };
};

describe('basic flow', () => {
  it('user can enter text', () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: 'hello' } });
    expect(input.value).toBe('hello');
  });

  it('entering text and pressing "search" returns users list', async () => {
    server.use(
      rest.get(API_URLS.SEARCH_USERS(), (_req, res, ctx) => {
        return res(
          ctx.status(200),
          ctx.json({
            total_count: 2,
            items: [
              {
                login: 'abc',
                id: 1,
              },
              {
                login: 'abcdef',
                id: 2,
              },
            ],
          }),
        );
      }),
    );

    const { input, searchButton, findByText } = setup();
    fireEvent.change(input, { target: { value: 'abc' } });
    fireEvent.click(searchButton);

    expect(await findByText('abcdef')).toBeVisible();
  });

  it('"load more" button paginates repositories', async () => {
    server.use(
      rest.get(API_URLS.SEARCH_USERS(), (_req, res, ctx) => {
        return res(
          ctx.json({
            total_count: 2,
            items: [
              {
                login: 'abc',
                id: 1,
              },
              {
                login: 'abcdef',
                id: 2,
              },
            ],
          }),
        );
      }),
      rest.get(API_URLS.USER_REPOS('abc'), (req, res, ctx) => {
        const page = req.url.searchParams.get('page');
        const data = Array.from({ length: 55 }).map((_v, idx) => ({
          id: idx,
          name: `repo${idx}`,
          description: `description${idx}`,
          url: `https://github.com/abc/${idx}`,
          stargazers_count: 15 + idx,
        }));
        const per_page = Number.parseInt(String(req.url.searchParams.get('per_page') ?? 30), 10);

        const json = page === '1' ? data.slice(0, per_page) : data.slice(per_page + 1, per_page * 2);

        return res(ctx.json(json));
      }),
    );

    const { input, searchButton, findByText, findByTestId } = setup();
    fireEvent.change(input, { target: { value: 'abc' } });
    fireEvent.click(searchButton);

    // click on user
    fireEvent.click(await findByText('abc'));

    // click 'load more' to get the second page
    fireEvent.click(await findByTestId('load-more'));

    expect(await findByText('repo45')).toBeVisible();
  });
});
