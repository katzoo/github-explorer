import { useState } from 'react';
import { Button, Stack, Text } from '@chakra-ui/react';
import { ChevronDownIcon } from '@chakra-ui/icons';
import { SearchUsersResults } from '~/domain/users';
import { UserReposList } from './UserReposList';

interface Props {
  data: SearchUsersResults['items'][0];
}

export const SearchUserListItem: React.FC<Props> = ({ data }) => {
  const [isExpanded, setIsExpanded] = useState(() => false);

  return (
    <Stack as="li" py={1} px={2} direction="column" flexGrow="1">
      <Button
        flexGrow="1"
        bg="gray.200"
        alignItems="center"
        justifyContent="space-between"
        onClick={() => {
          setIsExpanded((v) => !v);
        }}
      >
        <Text as="span">{data.login}</Text>
        <ChevronDownIcon
          sx={{
            transform: `rotate(${isExpanded ? 180 : 0}deg)`,
          }}
        />
      </Button>
      {isExpanded && <UserReposList username={data.login} />}
    </Stack>
  );
};
