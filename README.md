# github repositories explorer

[alluring-zebra.surge.sh](alluring-zebra.surge.sh)

## start

```
yarn
yarn dev
```

## start (prod)

```
yarn
yarn build && yarn preview
```

## limitations / issues

- no hardcoded api token, so requests might start to fail after using whole quota (github public rate limitter)
- not optimized for a11y
- bundle is over 160 KB (gzip),
