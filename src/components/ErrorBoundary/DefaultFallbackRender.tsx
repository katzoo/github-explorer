import React, { useEffect } from 'react';
import { Center, CenterProps, Text, Button, VStack } from '@chakra-ui/react';
import { FallbackProps } from 'react-error-boundary';

import { HttpError } from '~/utils/request';

export const DefaultFallbackRender: React.FC<FallbackProps & CenterProps> = ({
  error,
  resetErrorBoundary,
  ...centerProps
}) => {
  if (error instanceof HttpError || error instanceof TypeError) {
    return (
      <Center p={3} minHeight="200px" {...centerProps}>
        <VStack>
          <Text as="span">Failed to load data</Text>
          <Button onClick={() => resetErrorBoundary()} colorScheme="blue" size="sm">
            Try again
          </Button>
        </VStack>
      </Center>
    );
  }

  return (
    <Center p={3} width="100%" height="100%" minHeight="200px" {...centerProps}>
      <Text as="span">Something went wrong...</Text>
    </Center>
  );
};

export const defaultFallbackRender = (props: FallbackProps) => <DefaultFallbackRender {...props} />;
