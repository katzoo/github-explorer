import { useReducer } from 'react';

interface State {
  inputQuery: string;
  searchQuery: string;
}

type ActionTypes = { type: 'set-input-query'; query: string } | { type: 'copy-input-query' };

export const searchViewReducer = (state: State, action: ActionTypes): State => {
  switch (action.type) {
    case 'set-input-query':
      return {
        ...state,
        inputQuery: action.query,
      };
    case 'copy-input-query':
      return {
        ...state,
        searchQuery: state.inputQuery,
      };
    default:
      return state;
  }
};

const initialState: State = {
  inputQuery: '',
  searchQuery: '',
};

export const useSearchViewReducer = () => useReducer(searchViewReducer, initialState);
