import { SearchView } from '~/views/SearchView';
import './App.css';
import { AppProviders } from './AppProviders';

const App = () => (
  <AppProviders>
    <SearchView />
  </AppProviders>
);

export default App;
