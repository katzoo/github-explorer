import { useQuery } from '@tanstack/react-query';

import { searchUsers, SearchUsersResults } from '~/domain/users';

const dummyData: SearchUsersResults = {
  total_count: 0,
  items: [],
};

export const useSearchUsersData = (query: string) => {
  const { data = dummyData } = useQuery({
    queryKey: ['search-users', query],
    queryFn: () => searchUsers({ query: query }),
    enabled: query !== '',
    suspense: true,
  });

  return data;
};
