import { Flex, Stack, Text } from '@chakra-ui/react';
import { useQuery } from '@tanstack/react-query';
import { searchUsers, SearchUsersResults, UserRepos } from '~/domain/users';
import { SearchUserListItem } from './SearchUserListItem';
import { useSearchUsersData } from './useSearchUsersData';

interface Props {
  query: string;
}

export const SearchUsersList: React.FC<Props> = ({ query }) => {
  const data = useSearchUsersData(query);

  if (data.total_count === 0) {
    return <Text as="span">No results.</Text>;
  }

  return (
    <Stack as="ul">
      {data.items.map((item) => (
        <SearchUserListItem key={item.id} data={item} />
      ))}
    </Stack>
  );
};
