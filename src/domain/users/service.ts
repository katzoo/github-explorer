import { makeValidatedCancellableFetch } from '~/utils/request';
import { searchUsersResultsValidator, userReposValidator } from './types';

const API_URL_BASE = 'https://api.github.com';

export const API_URLS = {
  SEARCH_USERS: () => `${API_URL_BASE}/search/users`,
  USER_REPOS: (username: string) => `${API_URL_BASE}/users/${username}/repos`,
};

interface SearchUsersParams {
  query: string;
}

export const searchUsers = ({ query }: SearchUsersParams) => {
  const makeUsersFetch = makeValidatedCancellableFetch(searchUsersResultsValidator);
  const params = new URLSearchParams();

  params.set('q', query);
  params.set('per_page', '5');

  return makeUsersFetch(`${API_URLS.SEARCH_USERS()}?${params.toString()}`, {
    headers: {
      accept: 'application/vnd.github+json',
    },
  });
};

export interface GetUserReposParams {
  username: string;
  per_page?: number;
  page?: number;
}

export const getUserRepos = ({ username, per_page = 30, page = 1 }: GetUserReposParams) => {
  const makeReposFetch = makeValidatedCancellableFetch(userReposValidator);

  const params = new URLSearchParams();

  params.set('page', String(Number.isInteger(page) ? page : 1));
  params.set('per_page', String(Number.isInteger(per_page) ? per_page : 30));

  return makeReposFetch(`${API_URLS.USER_REPOS(username)}?${params.toString()}`, {
    headers: {
      accept: 'application/vnd.github+json',
    },
  });
};
