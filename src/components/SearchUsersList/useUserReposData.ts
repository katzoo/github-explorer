import { useQuery } from '@tanstack/react-query';

import { getUserRepos, GetUserReposParams, UserRepos } from '~/domain/users';

const dummyData: never[] = [];

export const useUserReposData = ({ username, page = 1, per_page = 30 }: GetUserReposParams): UserRepos => {
  const { data = dummyData } = useQuery({
    queryKey: ['user-repos', username, per_page, page],
    queryFn: () => getUserRepos({ username, per_page, page }),
    suspense: true,
  });

  return data;
};
