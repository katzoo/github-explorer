import { z } from 'zod';

export const searchUsersResultsValidator = z.object({
  total_count: z.number(),
  items: z.array(
    z.object({
      login: z.string(),
      id: z.number(),
    }),
  ),
});

export type SearchUsersResults = z.infer<typeof searchUsersResultsValidator>;

export const userReposValidator = z.array(
  z.object({
    id: z.number(),
    name: z.string(),
    description: z.string().nullable(),
    url: z.string().url(),
    stargazers_count: z.number(),
  }),
);
export type UserRepos = z.infer<typeof userReposValidator>;
