import { Suspense, useState } from 'react';
import { StarIcon } from '@chakra-ui/icons';
import { Button, Center, Flex, LinkBox, LinkOverlay, Spinner, Stack, Text } from '@chakra-ui/react';

import { GetUserReposParams, UserRepos } from '~/domain/users';
import { ErrorBoundary } from '~/components/ErrorBoundary';
import { useUserReposData } from './useUserReposData';

interface Props extends GetUserReposParams {}

type LoadMoreState = 'none' | 'load-more-button' | 'expanded';

const hasMoreData = (per_page: number) => (data: UserRepos) => data.length === per_page;

const UserReposListComponent: React.FC<Props> = ({ username, page = 1, per_page = 30 }) => {
  const data = useUserReposData({ username, page, per_page });
  const [loadMoreState, setLoadMoreState] = useState<LoadMoreState>(() =>
    hasMoreData(per_page)(data) ? 'load-more-button' : 'none',
  );

  if (page === 1 && data.length === 0) {
    return <Text as="span">User has no repositories.</Text>;
  }

  const content = data.map((d) => (
    <Flex key={d.id} as="li" pl={4}>
      <LinkBox px={2} py={3} bg="gray.300" w="100%">
        <Stack direction="column" gap={2}>
          <Flex>
            <Text
              as={LinkOverlay}
              flex="1 1 auto"
              minWidth={2}
              fontWeight="bold"
              href={d.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              {d.name}
            </Text>
            <Flex flex="0 0 auto" alignItems="center" gap={1}>
              <Text as="span" fontSize="sm" fontWeight="bold">
                {d.stargazers_count}
              </Text>
              <StarIcon fontSize="sm" />
            </Flex>
          </Flex>
          <Flex minH="1em">{d.description}</Flex>
        </Stack>
      </LinkBox>
    </Flex>
  ));

  const nestedRepos =
    loadMoreState === 'expanded' ? <UserReposList username={username} page={page + 1} per_page={per_page} /> : null;
  const loadMoreButton =
    loadMoreState === 'load-more-button' ? (
      <Center p={3}>
        <Button
          onClick={() => {
            setLoadMoreState('expanded');
          }}
          colorScheme="blue"
          data-testid="load-more"
        >
          Load more
        </Button>
      </Center>
    ) : null;

  if (page === 1) {
    return (
      <>
        <Stack as="ul" direction="column">
          {content}
          {nestedRepos}
        </Stack>
        {loadMoreButton}
      </>
    );
  }

  // NOTE(m.kania): we're already nested (load more pressed at least once)
  return (
    <>
      {content}
      {nestedRepos}
      {loadMoreButton}
    </>
  );
};

export const UserReposList: React.FC<Props> = (props) => (
  <ErrorBoundary>
    <Suspense
      fallback={
        <Center p={2}>
          <Spinner />
        </Center>
      }
    >
      <UserReposListComponent {...props} />
    </Suspense>
  </ErrorBoundary>
);
