import { describe, it, expect } from 'vitest';
import { server, rest } from '~/test/test-server';

import { API_URLS, searchUsers, getUserRepos } from '../service';

describe('searchUsers', () => {
  it('returns data in expected shape', async () => {
    server.use(
      rest.get(API_URLS.SEARCH_USERS(), (_req, res, ctx) => {
        return res(
          ctx.status(200),
          ctx.json({
            total_count: 2,
            items: [
              {
                login: 'abc',
                id: 1,
              },
              {
                login: 'abcd',
                id: 2,
              },
            ],
          }),
        );
      }),
    );
    const response = await searchUsers({ query: 'abc' });

    expect(response.total_count).toEqual(2);
    expect(response.items).toEqual(
      expect.objectContaining([
        {
          login: 'abc',
          id: 1,
        },
      ]),
    );
  });

  it('throws on invalid shape', async () => {
    server.use(
      rest.get(API_URLS.SEARCH_USERS(), (_req, res, ctx) => {
        return res(
          ctx.status(200),
          ctx.json({
            nope: 123,
          }),
        );
      }),
    );

    await expect(searchUsers({ query: 'abc' })).rejects.toThrow();
  });
});

describe('getUserRepos', () => {
  it('returns data in expected shape', async () => {
    server.use(
      rest.get(API_URLS.USER_REPOS('abc'), (_req, res, ctx) => {
        return res(
          ctx.status(200),
          ctx.json([
            {
              id: 1,
              name: 'a',
              description: 'desc',
              url: 'https://github.com/',
              stargazers_count: 2,
            },
          ]),
        );
      }),
    );
    const response = await getUserRepos({ username: 'abc' });

    expect(response[0]).toEqual(
      expect.objectContaining({
        name: 'a',
        id: 1,
      }),
    );
  });

  it('throws on invalid shape', async () => {
    server.use(
      rest.get(API_URLS.USER_REPOS('abc'), (_req, res, ctx) => {
        return res(ctx.status(200), ctx.json([{ not_id: 123 }]));
      }),
    );

    await expect(getUserRepos({ username: 'abc' })).rejects.toThrow();
  });
});
