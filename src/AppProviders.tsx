import { useMemo } from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ChakraProvider } from '@chakra-ui/react';

import { ErrorBoundary } from '~/components/ErrorBoundary';

interface Props {
  children?: React.ReactNode;
}

export const AppProviders: React.FC<Props> = ({ children }) => {
  const queryClient = useMemo(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            retry: false,
            refetchOnWindowFocus: false,
            useErrorBoundary: true,
          },
        },
      }),
    [],
  );

  return (
    <QueryClientProvider client={queryClient}>
      <ChakraProvider>
        <ErrorBoundary>{children}</ErrorBoundary>
      </ChakraProvider>
    </QueryClientProvider>
  );
};
