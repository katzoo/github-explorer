import { Button, Center, FormControl, FormLabel, Input, Spinner, Stack } from '@chakra-ui/react';
import React, { Suspense, useMemo } from 'react';
import { SearchUsersHeadline } from '~/components/SearchUsersHeadline';
import { SearchUsersList } from '~/components/SearchUsersList';
import { ErrorBoundary } from '~/components/ErrorBoundary';
import { useSearchViewReducer } from './useSearchViewReducer';

interface Props {}

export const SearchView: React.FC<Props> = () => {
  const [state, dispatch] = useSearchViewReducer();
  const isSearchButtonDisabled = state.inputQuery === '';
  const showResultsList = state.searchQuery !== '';

  const { onSubmit, onInputChange } = useMemo(
    () => ({
      onInputChange: (ev: React.ChangeEvent<HTMLInputElement>) => {
        ev.preventDefault();
        dispatch({ type: 'set-input-query', query: ev.target.value });
      },
      onSubmit: (ev: React.FormEvent<HTMLFormElement>) => {
        ev.preventDefault();
        dispatch({ type: 'copy-input-query' });
      },
    }),
    [dispatch],
  );

  return (
    <Stack
      direction="column"
      gap={2}
      p={3}
      bg="white"
      maxWidth={{ base: '100%', md: '768px' }}
      margin={{ md: '0 auto' }}
    >
      <form onSubmit={onSubmit}>
        <Stack direction="column" gap={1}>
          <FormControl>
            <FormLabel>Username</FormLabel>
            <Input
              type="text"
              onChange={onInputChange}
              value={state.inputQuery}
              placeholder="Enter username to search for"
            />
          </FormControl>
          <Button type="submit" colorScheme="blue" isDisabled={isSearchButtonDisabled}>
            Search
          </Button>
        </Stack>
      </form>
      {showResultsList && (
        <ErrorBoundary>
          <Suspense
            fallback={
              <Center p={3}>
                <Spinner />
              </Center>
            }
          >
            <Stack direction="column" gap={1}>
              <SearchUsersHeadline query={state.searchQuery} />
              <SearchUsersList key={state.searchQuery} query={state.searchQuery} />
            </Stack>
          </Suspense>
        </ErrorBoundary>
      )}
    </Stack>
  );
};
